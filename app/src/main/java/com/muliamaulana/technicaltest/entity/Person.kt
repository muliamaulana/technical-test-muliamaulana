package com.muliamaulana.technicaltest.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "person")
data class Person(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long = 0,
    @ColumnInfo(name = "NAME") var name: String,
    @ColumnInfo(name = "ADDRESS") var address: String,
    @ColumnInfo(name = "PHONE") var phone: String?
)
