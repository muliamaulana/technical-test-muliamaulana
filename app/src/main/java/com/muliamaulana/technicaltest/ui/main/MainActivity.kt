package com.muliamaulana.technicaltest.ui.main

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.muliamaulana.technicaltest.R
import com.muliamaulana.technicaltest.databinding.ActivityMainBinding
import com.muliamaulana.technicaltest.entity.Person
import com.muliamaulana.technicaltest.ui.history.HistoryActivity

class MainActivity : AppCompatActivity() {

    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by viewModels()
    private var isAllValid = false
    private var isUpdate = false

    companion object {
        var person: Person? = null
        fun launchIntent(activity: Activity, person: Person?) {
            this.person = person
            (activity as HistoryActivity).resultLauncher.launch(
                Intent(activity, MainActivity::class.java)
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        isUpdate = person != null


        binding.apply {

            inputName.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun afterTextChanged(edit: Editable?) {
                    if (!isAllValid && edit.toString().isEmpty()) {
                        inputLayoutName.isErrorEnabled = true
                        inputLayoutName.error =
                            "${getString(R.string.name)} ${getString(R.string.field_required_message)}"
                    } else inputLayoutName.isErrorEnabled = false
                }
            })

            inputAddress.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun afterTextChanged(edit: Editable?) {
                    if (!isAllValid && edit.toString().isEmpty()) {
                        inputLayoutAddress.isErrorEnabled = true
                        inputLayoutAddress.error =
                            "${getString(R.string.address)} ${getString(R.string.field_required_message)}"
                    } else inputLayoutAddress.isErrorEnabled = false
                }
            })

            inputPhoneNumber.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun afterTextChanged(edit: Editable?) {
                    if (!isAllValid && edit.toString().isEmpty()) {
                        inputLayoutPhoneNumber.isErrorEnabled = true
                        inputLayoutPhoneNumber.error =
                            "${getString(R.string.phone_number)} ${getString(R.string.field_required_message)}"
                    } else inputLayoutPhoneNumber.isErrorEnabled = false
                }
            })

            if (isUpdate) {
                supportActionBar?.title = getString(R.string.edit)
                supportActionBar?.setDisplayHomeAsUpEnabled(true)
                inputName.setText(person?.name)
                inputAddress.setText(person?.address)
                inputPhoneNumber.setText(person?.phone)
                buttonSave.text = getString(R.string.update)
            } else {
                supportActionBar?.title = getString(R.string.add_new)
            }

            buttonSave.setOnClickListener {

                if (isAllValid()) {
                    val name = inputName.text.toString()
                    val address = inputAddress.text.toString()
                    val phoneNumber = inputPhoneNumber.text.toString()
                    if (isUpdate) {
                        person?.name = name
                        person?.address = address
                        person?.phone = phoneNumber
                        viewModel.update(person)
                        setResult(RESULT_OK)
                        finish()
                    } else {
                        val newPerson = Person(name = name, address = address, phone = phoneNumber)
                        person = newPerson
                        viewModel.insertPerson(person)
                        inputLayoutName.editText?.text?.clear()
                        inputLayoutAddress.editText?.text?.clear()
                        inputLayoutPhoneNumber.editText?.text?.clear()

                        inputName.clearFocus()
                        inputAddress.clearFocus()
                        inputPhoneNumber.clearFocus()

                        Snackbar.make(binding.root, "${person?.name} Saved", Snackbar.LENGTH_SHORT)
                            .show()

                        val imm =
                            getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                        imm?.hideSoftInputFromWindow(binding.root.windowToken, 0)
                    }


                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (!isUpdate) {
            val inflater: MenuInflater = menuInflater
            inflater.inflate(R.menu.main_menu, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.history -> {
                startActivity(Intent(this, HistoryActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun isAllValid(): Boolean {

        isAllValid = true
        binding.apply {
            inputLayoutName.isErrorEnabled = false
            inputLayoutAddress.isErrorEnabled = false
            inputLayoutPhoneNumber.isErrorEnabled = false

            if (inputName.text.isNullOrEmpty() && inputName.text.isNullOrBlank()) {
                inputLayoutName.isErrorEnabled = true
                inputLayoutName.error =
                    "${getString(R.string.name)} ${getString(R.string.field_required_message)}"
                isAllValid = false
                return isAllValid
            }

            if (inputAddress.text.isNullOrEmpty() && inputAddress.text.isNullOrBlank()) {
                inputLayoutAddress.isErrorEnabled = true
                inputLayoutAddress.error =
                    "${getString(R.string.address)} ${getString(R.string.field_required_message)}"
                isAllValid = false
                return isAllValid
            }

            if (inputPhoneNumber.text.isNullOrEmpty() && inputPhoneNumber.text.isNullOrBlank()) {
                inputLayoutPhoneNumber.isErrorEnabled = true
                inputLayoutPhoneNumber.error =
                    "${getString(R.string.phone_number)} ${getString(R.string.field_required_message)}"
                isAllValid = false
                return isAllValid
            }
        }

        return isAllValid
    }
}