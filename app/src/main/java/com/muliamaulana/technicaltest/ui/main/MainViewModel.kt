package com.muliamaulana.technicaltest.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.muliamaulana.technicaltest.db.PersonDao
import com.muliamaulana.technicaltest.db.PersonRoomDatabase
import com.muliamaulana.technicaltest.entity.Person

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private var dao: PersonDao = PersonRoomDatabase.getDatabase(getApplication()).personDao()

    fun insertPerson(person: Person?) {
        dao.insert(person)
    }

    fun update(person: Person?) {
        dao.update(person)
    }

}