package com.muliamaulana.technicaltest.ui.history

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muliamaulana.technicaltest.databinding.ItemPersonLayoutBinding
import com.muliamaulana.technicaltest.entity.Person


class PersonAdapter(private val callback: Callback) :
    RecyclerView.Adapter<PersonAdapter.ViewHolder>() {

    private var data: MutableList<Person> = mutableListOf()
    fun setData(data: MutableList<Person>) {
        this.data = data
    }

    interface Callback {
        fun onItemClickListener(person: Person)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemPersonLayoutBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ViewHolder(binding, callback)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(data[position])

    override fun getItemCount(): Int = data.size

    class ViewHolder(private val binding: ItemPersonLayoutBinding, private val callback: Callback) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Person) {
            binding.apply {
                textViewName.text = item.name
                textViewAddress.text = item.address
                textViewPhone.text = item.phone
                cardView.setOnClickListener {
                    callback.onItemClickListener(item)
                }
            }
        }
    }
}