package com.muliamaulana.technicaltest.ui.history

import android.app.Activity
import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.AbsListView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.muliamaulana.technicaltest.*
import com.muliamaulana.technicaltest.databinding.ActivityHistoryBinding
import com.muliamaulana.technicaltest.entity.Person
import com.muliamaulana.technicaltest.ui.main.MainActivity


class HistoryActivity : AppCompatActivity(), PersonAdapter.Callback {
    private var _binding: ActivityHistoryBinding? = null
    private val binding get() = _binding!!

    private val viewModel: HistoryViewModel by viewModels()
    private lateinit var adapter: PersonAdapter
    private var list = mutableListOf<Person>()

    private var recentlyDeletedPosition = -1
    private var recentlyDeletedPerson: Person? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityHistoryBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.title = getString(R.string.history)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

        adapter = PersonAdapter(this)
        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(binding.recyclerViewPersons)

        viewModel.getAllPersons()
        viewModel.allPersons.observe(this, { results ->
            list = results
            adapter.setData(list)

            binding.apply {
                recyclerViewPersons.isVisible = !list.isNullOrEmpty()
                textViewNoData.isVisible = list.isNullOrEmpty()

                recyclerViewPersons.layoutManager = LinearLayoutManager(this@HistoryActivity)
                recyclerViewPersons.adapter = adapter
            }
        })

        binding.apply {
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(newText: String): Boolean {
                    if (newText.length > 2) {
                        viewModel.search(newText)
                    } else if (newText.isEmpty()) {
                        viewModel.getAllPersons()
                    }
                    return false
                }

                override fun onQueryTextSubmit(query: String): Boolean {
                    return false
                }
            })

            recyclerViewPersons.addOnScrollListener(object : RecyclerView.OnScrollListener() {

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (newState == AbsListView.OnScrollListener.SCROLL_STATE_FLING) {
                        imm?.hideSoftInputFromWindow(binding.root.windowToken, 0)
                    }
                }
            })
        }
    }

    private var simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object :
        ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {


        override fun onChildDraw(
            c: Canvas,
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            dX: Float,
            dY: Float,
            actionState: Int,
            isCurrentlyActive: Boolean
        ) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            val background =
                ColorDrawable(
                    ContextCompat.getColor(
                        this@HistoryActivity,
                        R.color.colorPrimaryVariantLight
                    )
                )
            val icon = ContextCompat.getDrawable(this@HistoryActivity, R.drawable.ic_delete)!!

            val itemView = viewHolder.itemView
            val backgroundCornerOffset = 20

            val iconMargin = 100
            val iconTop = itemView.top + (itemView.height - icon.intrinsicHeight) / 2
            val iconBottom = iconTop + icon.intrinsicHeight

            if (dX < 0) { // Swiping to the left
                val iconLeft = itemView.right - iconMargin - icon.intrinsicWidth
                val iconRight = itemView.right - iconMargin
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom)
                background.setBounds(
                    itemView.right + dX.toInt() - backgroundCornerOffset,
                    itemView.top, itemView.right, itemView.bottom
                )
            } else { // view is unSwiped
                background.setBounds(0, 0, 0, 0)
            }
            background.draw(c)
            icon.draw(c)
        }

        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            Toast.makeText(this@HistoryActivity, "on Move", Toast.LENGTH_SHORT).show()
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
            //Remove swiped item from list and notify the RecyclerView
            val position = viewHolder.adapterPosition
            val snackBar =
                Snackbar.make(binding.root, "${list[position].name} Deleted", Snackbar.LENGTH_SHORT)
            snackBar.setAction(getString(R.string.Undo)) { undoDelete() }
            snackBar.show()

            snackBar.addCallback(object : Snackbar.Callback() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    super.onDismissed(transientBottomBar, event)
                    if (list.isEmpty()) {
                        viewModel.getAllPersons()
                    }
                }

                override fun onShown(sb: Snackbar?) {
                    super.onShown(sb)
                }
            })
            recentlyDeletedPerson = list[position]
            recentlyDeletedPosition = position
            viewModel.delete(list[position])
            adapter.notifyItemRemoved(position)
            list.removeAt(position)

        }
    }

    private fun undoDelete() {
        if (recentlyDeletedPerson != null) {
            list.add(recentlyDeletedPosition, recentlyDeletedPerson!!)
            adapter.notifyItemInserted(recentlyDeletedPosition)

            viewModel.insert(recentlyDeletedPerson!!)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onItemClickListener(person: Person) {
        MainActivity.launchIntent(this, person)
    }

    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                viewModel.getAllPersons()
            }
        }
}