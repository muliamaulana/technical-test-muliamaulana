package com.muliamaulana.technicaltest.ui.history

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.muliamaulana.technicaltest.db.PersonDao
import com.muliamaulana.technicaltest.db.PersonRoomDatabase
import com.muliamaulana.technicaltest.entity.Person

class HistoryViewModel(application: Application) : AndroidViewModel(application) {

    private var dao: PersonDao = PersonRoomDatabase.getDatabase(getApplication()).personDao()

    var allPersons: MutableLiveData<MutableList<Person>> = MutableLiveData()
    fun getAllPersons() {
        allPersons.value = dao.getAll()
    }

    fun search(query: String) {
        allPersons.value = dao.search(query)
    }

    fun delete(person: Person) {
        dao.delete(person)
    }

    fun insert(person: Person) {
        dao.insert(person)
    }

}