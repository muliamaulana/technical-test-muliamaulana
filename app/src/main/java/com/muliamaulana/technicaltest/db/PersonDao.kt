package com.muliamaulana.technicaltest.db

import androidx.room.*
import com.muliamaulana.technicaltest.entity.Person

@Dao
interface PersonDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(person: Person?)

    @Update
    fun update(person: Person?)

    @Delete
    fun delete(person: Person)

    @Query("SELECT * FROM person ORDER BY id DESC")
    fun getAll(): MutableList<Person>

    @Query("SELECT * FROM person WHERE name LIKE '%' || :query || '%' OR (address LIKE '%' || :query || '%') OR (phone LIKE '%' || :query ||'%')")
    fun search(query: String): MutableList<Person>

}