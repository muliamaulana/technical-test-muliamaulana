package com.muliamaulana.technicaltest.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.muliamaulana.technicaltest.entity.Person

@Database(entities = [Person::class], version = 1, exportSchema = false)
abstract class PersonRoomDatabase : RoomDatabase() {

    companion object {
        @Volatile
        private var INSTANCE: PersonRoomDatabase? = null

        fun getDatabase(context: Context): PersonRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PersonRoomDatabase::class.java,
                    "person_db"
                )
                    .allowMainThreadQueries() // allows Room to executing task in main thread
                    .fallbackToDestructiveMigration() // allows Room to recreate table if no migrations found
                    .build()

                INSTANCE = instance
                instance
            }
        }
    }

    abstract fun personDao(): PersonDao
}